const http = require("http");

const webSocket = require("ws");

const server = http.createServer((req, res) => {
  res.end("I am Connected");
});

const wss = new webSocket.Server({ server });

wss.on("headers", (headers, req) => {
  console.log(headers);
});

wss.on("connection", (ws, req) => {
  ws.send("Welcome to the server!");
});

server.listen("8000");
