const express = require("express");
const app = express();
const socketio = require("socket.io");

let namespaces = require("./data/namespaces");
// console.log(namespace);

app.use(express.static(__dirname + "/public"));

const expressServer = app.listen(9000);
const io = socketio(expressServer);

// Connection of Main Namespace
io.on("connection", (socket) => {
  //build an array to send back with an img  and ednpoint of each namespace
  let nsData = namespaces.map((ns) => {
    return {
      img: ns.img,
      endpoint: ns.endpoint,
    };
  });

  // sned nsData to client
  socket.emit("nsList", nsData);
});

// Loop Through namespace and listen for connection
namespaces.forEach((namespace) => {
  io.of(namespace.endpoint).on("connection", (nsSocket) => {
    const userName = nsSocket.handshake.query.username || "User";
    // console.log(`${nsSocket.id} has joined namespace ${namespace.endpoint}`);
    // a nsSocket has connected to one ns
    //send that namespace group info back
    nsSocket.emit("nsRoomLoad", namespace.rooms);
    nsSocket.on("joinRoom", (roomToJoin, numberOfMembersCallback) => {
      // Deal with History
      const roomToLeave = Object.keys(nsSocket.rooms)[1];
      nsSocket.leave(roomToLeave);
      updateUsersInRoom(namespace, roomToLeave);
      nsSocket.join(roomToJoin);
      io.of(namespace.endpoint)
        .in(roomToJoin)
        .clients((err, clients) => {
          // console.log(clients.length);
          numberOfMembersCallback(clients.length);
        });

      const nsRoom = namespace.rooms.find(
        (obj) => obj.roomTitle === roomToJoin
      );
      nsSocket.emit("historyCatchup", nsRoom.history);
      // Send Back number user to This room
      updateUsersInRoom(namespace, roomToJoin);
    });

    nsSocket.on("newMessageToServer", (message) => {
      const fullMessage = {
        text: message.text,
        time: Date.now(),
        username: userName,
        avatar: "https://via.placeholder.com/30",
      };

      // console.log(`${message}`);
      // Send this Message to all the socket connected
      // console.log(nsSocket.rooms);
      // User will be in the second roo, of the object
      // get key
      const roomTitle = Object.keys(nsSocket.rooms)[1];

      // We need to find our room
      // console.log(namespaces[0].rooms);
      const nsRoom = namespace.rooms.find((obj) => obj.roomTitle === roomTitle);

      nsRoom.addMessage(fullMessage);

      io.of(namespace.endpoint)
        .to(roomTitle)
        .emit("messageToClients", fullMessage);
    });
  });
});

function updateUsersInRoom(namespace, roomToJoin) {
  io.of(namespace.endpoint)
    .in(roomToJoin)
    .clients((err, clients) => {
      io.of(namespace.endpoint)
        .in(roomToJoin)
        .emit("updateMembers", clients.length);
    });
}
