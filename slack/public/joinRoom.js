function joinRoom(roomName) {
  // Send this to Server
  nsSocket.emit("joinRoom", roomName, (newNumberOfUsers) => {
    // We want to update room member total;

    document.querySelector(
      ".curr-room-num-users"
    ).innerHTML = `${newNumberOfUsers} <span class="glyphicon glyphicon-user"></span>`;
  });

  nsSocket.on("historyCatchup", (history) => {
    const messsageUl = document.querySelector("#messages");
    messsageUl.innerHTML = "";

    history.forEach((message) => {
      const newMessage = buildHTML(message);
      const currentMessage = messsageUl.innerHTML;
      messsageUl.innerHTML = currentMessage + newMessage;
    });
    messsageUl.scrollTo(0, messsageUl.scrollHeight);
  });

  nsSocket.on("updateMembers", (newNumberOfUsers) => {
    // We want to update room member total;

    document.querySelector(
      ".curr-room-num-users"
    ).innerHTML = `${newNumberOfUsers} <span class="glyphicon glyphicon-user"></span>`;

    document.querySelector(".curr-room-text").innerText = `${roomName}`;
  });

  let searchBox = document.querySelector("#search-box");
  searchBox.addEventListener("input", (e) => {
    let messages = Array.from(document.getElementsByClassName("message-text"));
    messages.forEach((msg) => {
      if (
        msg.innerText.toLowerCase().indexof(e.target.value.toLowerCase()) === -1
      ) {
        msg.style.display = "none";
      } else {
        msg.style.display = "block";
      }
    });
  });
}
