function joinNs(endpoint) {
  if (nsSocket) {
    nsSocket.close();
    // remove event listeners
    document
      .querySelector("#user-message")
      .removeEventListener("submit", formSubmission);
  }
  nsSocket = io(`http://localhost:9000${endpoint}`);
  nsSocket.on("nsRoomLoad", (nsRoom) => {
    let roomList = document.querySelector(".room-list");
    roomList.innerHTML = "";
    nsRoom.forEach((room) => {
      let glyph;

      if (room.privateRoom) {
        glyph = "lock";
      } else {
        glyph = "globe";
      }
      roomList.innerHTML += `<li class="room"><span class="glyphicon glyphicon-${glyph}"></span>${room.roomTitle}</li>`;
    });
    // Add Event Listner to Each Room Listne
    let roomNodes = document.getElementsByClassName("room");
    Array.from(roomNodes).forEach((el) => {
      el.addEventListener("click", (e) => {
        console.log(`Someone click on ${e.target.innerText}`);
        joinRoom(e.target.innerText);
      });
    });

    //    add User to the Room
    const topRoom = document.querySelector(".room");
    const topRoomName = topRoom.innerText;
    joinRoom(topRoomName);
  });

  nsSocket.on("messageToClients", (msg) => {
    const newMsg = buildHTML(msg);
    document.querySelector("#messages").innerHTML += newMsg;
  });

  document
    .querySelector(".message-form")
    .addEventListener("submit", formSubmission);
}

function formSubmission(event) {
  event.preventDefault();
  const newMessage = document.querySelector("#user-message").value;
  console.log("Form submitted");
  nsSocket.emit("newMessageToServer", { text: newMessage });
}

function buildHTML(msg) {
  const newDate = new Date(msg.time).toLocaleString();
  const newHTML = `<li>
    <div class="user-image">
        <img src="${msg.avatar}" />
    </div>
    <div class="user-message">
        <div class="user-name-time">${msg.username} <span>${newDate}</span></div>
        <div class="message-text">${msg.text}</div>
    </div>
</li>`;

  return newHTML;
}
