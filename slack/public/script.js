const username = prompt(`What's You Name?`);
// const socket = io("http://localhost:9000");
const socket = io("http://localhost:9000", {
  query: {
    username: username,
  },
});
let nsSocket = "";
socket.on("messageFromServer", (dataFromServer) => {
  // console.log(dataFromServer);
  socket.emit("messageToServer", { data: "This is form Client" });
});

socket.on("nsList", (nsData) => {
  let namespacesDiv = document.querySelector(".namespaces");
  namespacesDiv.innerHTML = "";
  nsData.forEach((ns) => {
    namespacesDiv.innerHTML += `<div class="namespace" ns=${ns.endpoint}><img src='${ns.img}'/></div>`;
  });

  // add Event listner ofr each namespace
  Array.from(document.getElementsByClassName("namespace")).forEach((el) => {
    el.addEventListener("click", (e) => {
      const nsEndpoint = el.getAttribute("ns");
      // console.log(nsEndpoint);
      joinNs(nsEndpoint);
    });
  });

  joinNs("/wiki");
});
