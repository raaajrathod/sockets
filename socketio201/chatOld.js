const express = require("express");

const app = express();
const socketio = require("socket.io");

app.use(express.static(__dirname + "/public"));

const expressServer = app.listen(9000);
const io = socketio(expressServer);

app.get("/ping", (req, res) => {
  res.send("Pong");
});

io.on("connection", (socket) => {
  socket.emit("messageFromServer", { data: "Welcome to Socket Server" });
  socket.on("messageToServer", (dataFromClient) => {
    console.log(dataFromClient);
  });

  socket.on("newMessageToServer", (msg) => {
    io.emit("messageToClients", { text: msg.text });
  });
});

io.of("/admin").on("connection", () => {
  console.log("Someone connected");
});
