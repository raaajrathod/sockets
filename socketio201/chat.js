const express = require("express");
const app = express();
const socketio = require("socket.io");

app.use(express.static(__dirname + "/public"));

const expressServer = app.listen(9000);
const io = socketio(expressServer);

// io.on() =  io.on('/').on('connection', ()=>{})
io.on("connection", (socket) => {
  socket.emit("messageFromServer", { data: "Welcome to Socket Server" });
  socket.on("messageToServer", (dataFromClient) => {
    console.log(dataFromClient);
  });

  socket.join("level1");
  socket
    .to("level1")
    .emit("joined", `${socket.id} says I have Joined Level 1 Room`);
});

io.of("/admin").on("connection", (socket) => {
  console.log("Someone connected");
  io.of("/admin").emit("welcome", "Welcome from admin namespace");
});
