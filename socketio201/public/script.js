const socket = io("http://localhost:9000");
const socket2 = io("http://localhost:9000/admin"); // /admin namespace
socket.on("messageFromServer", (dataFromServer) => {
  console.log(dataFromServer);
  socket.emit("messageToServer", { data: "This is form Client" });
});

socket2.on("welcome", (dataFromServer) => {
  console.log(dataFromServer);
});

socket.on("joined", (msg) => {
  console.log(msg);
});

document
  .querySelector("#message-form")
  .addEventListener("submit", function (event) {
    event.preventDefault();
    const newMessage = document.querySelector("#user-message").value;
    console.log("Form submitted");

    socket.emit("newMessageToServer", { text: newMessage });
  });
