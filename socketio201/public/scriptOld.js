const socket = io("http://localhost:9000");
const socket2 = io("http://localhost:9000/admin"); // /admin namespace
socket.on("messageFromServer", (dataFromServer) => {
  console.log(dataFromServer);
  socket.emit("messageToServer", { data: "This is form Client" });
});

document
  .querySelector("#message-form")
  .addEventListener("submit", function (event) {
    event.preventDefault();
    const newMessage = document.querySelector("#user-message").value;
    console.log("Form submitted");

    socket.emit("newMessageToServer", { text: newMessage });
  });

socket.on("messageToClients", (msg) => {
  document.querySelector("#messages").innerHTML += `<li>${msg.text}</li>`;
});

socket.on("connect", () => {
  console.log(socket.id);
});

socket2.on("connect", () => {
  console.log(socket2.id);
});
