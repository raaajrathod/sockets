// ==========================
// ==== Drawing ============
// =========================

player.locX = Math.floor(500 * Math.random() + 10);
player.locY = Math.floor(500 * Math.random() + 10);
// console.log(player.locX);
function draw() {
  // Set Translation back to Default
  context.setTransform(1, 0, 0, 1, 0, 0);

  // Clearing Screen
  context.clearRect(0, 0, canvas.width, canvas.height);

  // Clamp the cammera to the player
  const camX = -player.locX + canvas.width / 2;
  const camY = -player.locY + canvas.height / 2;
  context.translate(camX, camY);

  // Draw Orbs
  orbs.forEach((orb) => {
    context.beginPath();
    context.fillStyle = orb.color;
    context.arc(orb.locX, orb.locY, orb.radius, 0, Math.PI * 2);
    context.fill();
  });

  players.forEach((p) => {
    context.beginPath();
    context.fillStyle = p.color;
    // arg1,2 = x ,y
    // arg3 = radius
    // arg4 = where to start 0 3'o clock
    context.arc(p.locX, p.locY, p.radius, 0, Math.PI * 2);
    context.fill();
    context.lineWidth = 3;
    context.strokeStyle = "rgb(0,255,0)";
    context.stroke();
  });

  // Call the function passed to it, at the browser capacity, aiming for 30fps
  requestAnimationFrame(draw);
}
