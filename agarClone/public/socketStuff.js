let socket = io.connect("http://localhost:8080");

socket.on("initReturn", (data) => {
  // console.log(data);
  orbs = data.orbs;
  setInterval(() => {
    if (player.xVector && player.yVector) {
      // console.log("tick");
      socket.emit("tick", {
        xVector: player.xVector,
        yVector: player.yVector,
      });
    }
  }, 33);
});

function init() {
  draw();
  //   console.log(orbs);
  socket.emit("init", {
    playerName: player.name,
  });
}

socket.on("tock", (data) => {
  // console.log("tock");
  players = data.players;
});

socket.on("tickTock", (data) => {
  // console.log("tock");

  player.locX = data.playerX;
  player.locY = data.playerY;
});

socket.on("orbSwitch", (data) => {
  // console.log(data);
  orbs.splice(data.orbIndex, 1, data.newOrb);
});

socket.on("updateLeaderBoard", (data) => {
  // console.log(data);
  let leaderboard = "";
  data.map((obj) => {
    leaderboard += ` <li class="leaderboard-player">${obj.name} - ${obj.score}</li>`;
  });

  $(".leader-board").html(leaderboard);
});

socket.on("playerDeathEvent", (data) => {
  $("#game-message").html(
    `${data.died.name} absorbed by ${data.killedBy.name}`
  );
  $("#game-message").css({ "background-color": "#eee", opacity: 1 });
  $("#game-message").show();
  $("#game-message").fadeOut(5000);
});
