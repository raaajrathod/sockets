// All Main Sockett Stuff
const io = require("../servers").io;
const Orb = require("./classes/Orb");
const checkForOrbCollisions = require("./checkCollisions")
  .checkForOrbCollisions;
const checkForPlayerCollisions = require("./checkCollisions")
  .checkForPlayerCollisions;

// ======= CLASSES =================================
const Player = require("./classes/Player");
const PlayerData = require("./classes/PlayerData");
const PlayerConfig = require("./classes/PlayerConfig");
let orbs = [];
let players = [];
let settings = {
  defaultNumberOfOrbs: 50,
  defaultSpeed: 6,
  defaultSize: 6,
  defaultZoom: 1.5, // As player gets bigger Zoom out the screen
  worldWidth: 500,
  worldHeight: 500,
};

initGame();
setInterval(() => {
  if (players.length) {
    io.to("game").emit("tock", {
      players,
    });
  }
}, 33); // There are 30 33 in 1000 milliseconds, or 1/30th of a second,or 1 of 30 FPS

io.sockets.on("connect", (socket) => {
  let player = {};
  player.tickSent = false;
  console.log("Connected");
  socket.on("init", (data) => {
    // Add Player to game namespace
    socket.join("game");
    // Maake player config
    let playerConfig = new PlayerConfig(settings);
    let playerData = new PlayerData(data.playerName, settings);

    player = new Player(socket.id, playerConfig, playerData);
    // console.log(player);
    // issue a message to this client SOcket every 30fps
    setInterval(() => {
      if (player.tickSent) {
        socket.emit("tickTock", {
          playerX: player.playerData.locX,
          playerY: player.playerData.locY,
        });
      }
    }, 33); // There are 30 33 in 1000 milliseconds, or 1/30th of a second,or 1 of 30 FPS

    socket.emit("initReturn", {
      orbs,
    });
    players.push(playerData);

    socket.on("tick", (data) => {
      speed = player.playerConfig.speed;
      player.tickSent = true;
      //   console.log(data);
      // Update player config object with new data
      xV = player.playerConfig.xVector = data.xVector;
      yV = player.playerConfig.yVector = data.yVector;

      if (
        (player.playerData.locX < 5 && player.playerConfig.xVector < 0) ||
        (player.playerData.locX > settings.worldWidth && xV > 0)
      ) {
        player.playerData.locY -= speed * yV;
      } else if (
        (player.playerData.locY < 5 && yV > 0) ||
        (player.playerData.locY > settings.worldHeight && yV < 0)
      ) {
        player.playerData.locX += speed * xV;
      } else {
        player.playerData.locX += speed * xV;
        player.playerData.locY -= speed * yV;
      }
      // console.log(`${player.playerData.locX}`);
      // console.log(`${player.playerData.locX}, ${player.playerData.locY}`);

      /// ORB Collision
      let captureOrb = checkForOrbCollisions(
        player.playerData,
        player.playerConfig,
        orbs,
        settings
      );
      captureOrb
        .then((data) => {
          // Resolve will Run when collision happens
          console.log("Collision Happends");
          // emit to all socket
          const orbData = {
            orbIndex: data,
            newOrb: orbs[data],
          };
          // Every Socket needs to know leaderboard
          socket.emit("updateLeaderBoard", getLeaderboard());
          socket.emit("orbSwitch", orbData);
          // getLeaderboard();
        })
        .catch(() => {
          // Catch Runs when there is no collision
          // console.log("no collision");
        });

      // Player Collision
      let playerDeath = checkForPlayerCollisions(
        player.playerData,
        player.playerConfig,
        players,
        player.socketId
      );

      playerDeath
        .then((data) => {
          // Player Collsoiin
          // console.log("Player Collsoiin");
          socket.emit("updateLeaderBoard", getLeaderboard());
          socket.emit("playerDeathEvent", data);
        })
        .catch(() => {
          // No Player Collision
        });
    });
  });

  socket.on("disconnect", (data) => {
    // Find out Who left
    // Make Sure player has connected to the game
    if (player.playerData) {
      players.forEach((currPlayer, i) => {
        // if They Match
        if (currPlayer.uid === player.playerData.uid) {
          players.splice(i, 1);
          socket.emit("updateLeaderBoard", getLeaderboard());
        }
      });
    }
    const updateStats = "";
  });
});

function getLeaderboard() {
  // sort player is decending order
  players.sort(function (a, b) {
    return b.score - a.score;
  });
  // console.log(players);
  let leaderboard = players.map((currPlayer) => {
    return { name: currPlayer.name, score: currPlayer.score };
  });
  // console.log(leaderboard);
  return leaderboard;
}

// Run at Beginning of new game!
function initGame() {
  for (let i = 0; i < settings.defaultNumberOfOrbs; i++) {
    orbs.push(new Orb(settings));
  }
}

module.exports = io;
