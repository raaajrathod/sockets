// All Data is strored of GIven player

class Player {
  constructor(socketId, playerConfig, playerData) {
    this.socketId = socketId;
    this.uid = socketId;
    this.playerConfig = playerConfig;
    this.playerData = playerData;
  }
}

module.exports = Player;
