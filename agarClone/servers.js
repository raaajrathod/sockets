// Only JOb to make Socket and express Server, Thats it!
const express = require("express");

const app = express();
app.use(express.static(__dirname + "/public"));
// Require Socket
const socketio = require("socket.io");
// Create Express Server and listen on port 8080
const expressServer = app.listen(8080);
// Give that Port to Socket for Socket Connections
const io = socketio(expressServer);

const helmet = require("helmet");
app.use(helmet());
console.log("Express and Socket running on 8080");

// App Organization
module.exports = { app, io };
