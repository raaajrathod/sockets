// We need HTTP because we dont have express
const http = require("http");
// We need socketio its 3rd Party
const socketio = require("socket.io");
// Make HTTP server with Node
const server = http.createServer((req, res) => {
  res.end("I am Connected");
});

const io = socketio(server);

io.on("connection", (socket, req) => {
  // Change send to emit
  socket.emit("welcome", "Welcome to the server!");

  socket.on("message", (msg) => {
    console.log(msg);
  });
});

// listen on PORT 8000
server.listen("8000");
