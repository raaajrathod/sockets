// This File Captures performance Data
// and sends it up to socket.io server.

// What do we need to know about node performance
// - CPU load
// - Memory Usage (free and Total)
// - OS TYpe
// - Process Information

const os = require("os");
const cpus = os.cpus();

const io = require("socket.io-client");
let socket = io("http://localhost:8181");

socket.on("connect", () => {
  // console.log("I made connection to Workeers");

  //  We need a way to identify machine
  const nI = os.networkInterfaces();
  let macA;
  // Loop through all network interface for all the machine to get not internal macAddress
  for (let key in nI) {
    if (!nI[key][0].internal) {
      macA = nI[key][0].mac;
      break;
    }
  }
  // Basic Authentication
  socket.emit("clientAuth", "asdjashiajbciqwrb");

  // emiting new Event for Storing Data
  performanceData().then((data) => {
    data.macA = macA;
    socket.emit("initPerfData", data);
  });

  // Send Performance Data every one second
  let perfDataInterval = setInterval(() => {
    performanceData().then((data) => {
      data.macA = macA;
      socket.emit("perfData", data);
      // console.log("data Emited");
    });
  }, 1000);

  socket.on("disconnect", () => {
    clearInterval(perfDataInterval);
  });
});

function performanceData() {
  return new Promise(async (resolve, reject) => {
    // const osType = os.type() == "Darwin" ? "MacOS" : os.type();
    // console.log(osType);
    const osType = os.type() == "Darwin" ? "MacOS" : os.type();
    // console.log(osType);

    const upTime = os.uptime(); // Secs
    // console.log(upTime);

    const freeMem = os.freemem();
    // console.log(memUsage);

    const totalMem = os.totalmem();
    // console.log(totalMem);

    const usedMem = totalMem - freeMem;

    const memUsage = Math.floor((usedMem / totalMem) * 100) / 100;

    const cpuModel = cpus[0].model;
    const cpuSpeed = cpus[0].speed;
    const numberOfCore = cpus.length;
    // console.log(cpuModel);

    const cpuLoad = await getCPULoad();
    const isActive = true;
    resolve({
      freeMem,
      osType,
      upTime,
      totalMem,
      usedMem,
      memUsage,
      cpuModel,
      cpuSpeed,
      numberOfCore,
      cpuLoad,
      isActive,
    });
  });
}

// We need average of all the core to give average of all the core -util-is

function cpuAverage() {
  // Get Fresh Data
  const cpus = os.cpus();
  // get ms in each mode, since reboot
  // so get in now, and get it in 100 ms
  let idleMs = 0;
  let totalMs = 0;

  cpus.forEach((aCore) => {
    // Loop Through all the keys
    for (type in aCore.times) {
      totalMs += aCore.times[type];
    }
    idleMs += aCore.times.idle;
  });

  return {
    idle: idleMs / cpus.length,
    total: totalMs / cpus.length,
  };
}

function getCPULoad() {
  return new Promise((resolve, reject) => {
    const start = cpuAverage();
    setTimeout(() => {
      const end = cpuAverage();
      const idleDifference = end.idle - start.idle;
      const totallDifference = end.total - start.total;
      // console.log(`${idleDifference} ${totallDifference}`);
      // calculate percentage of CPU Usage
      const percentage =
        100 - Math.floor((100 * idleDifference) / totallDifference);
      resolve(percentage);
    }, 100);
  });
}
