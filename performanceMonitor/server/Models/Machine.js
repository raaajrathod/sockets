const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const Machine = new Schema({
  macA: String,
  freeMem: Number,
  osType: String,
  upTime: Number,
  totalMem: Number,
  usedMem: Number,
  memUsage: Number,
  cpuModel: String,
  cpuSpeed: Number,
  numberOfCore: Number,
  cpuLoad: Number,
});
module.exports = mongoose.model("Machine", Machine);
