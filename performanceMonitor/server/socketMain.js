const mongoose = require("mongoose");
mongoose.connect(
  "mongodb+srv://raj:raj@perfmonitor.fvyaf.mongodb.net/perfMonitor?retryWrites=true&w=majority",
  { useNewUrlParser: true, useUnifiedTopology: true }
);
const Machine = require("./Models/Machine");

function socketMain(io, socket) {
  let macA;
  console.log(`Some called me, I am connected - ${socket.id}`);

  socket.on("clientAuth", (key) => {
    if (key === "asdjashiajbciqwrb") {
      // valid Node Client
      socket.join("clients");
    } else if (key === "asdqwniiniuer") {
      // Valid UI Client
      socket.join("ui");
      console.log("React Client Joined");
      Machine.find({}, (err, docs) => {
        docs.forEach((machine) => {
          machine.isActive = false;
          io.to("ui").emit("data", machine);
        });
      });
    } else {
      // Invalid Client
      socket.disconnect(true);
    }
  });

  socket.on("disconnect", () => {
    Machine.find({ macA: macA }, (err, docs) => {
      if (docs.length) {
        docs[0].isActive = false;
        io.to("ui").emit("data", docs[0]);
      }
    });
  });

  // A machine is connected, check to see if its new or not

  socket.on("initPerfData", async (data) => {
    // console.log(data);
    macA = data.macA;
    let mongoResponse = await checkAndAdd(macA, data);
  });

  socket.on("perfData", (data) => {
    console.log("Tick...");
    io.to("ui").emit("data", data);
  });
}

function checkAndAdd(macA, data) {
  return new Promise((resolve, reject) => {
    Machine.findOne({ macA }, (err, machine) => {
      if (err) {
        throw err;
        reject(err);
      } else if (machine === null) {
        // If not found,
        let newMachine = new Machine(data);
        newMachine.save();
        resolve(newMachine);
      } else {
        resolve(machine);
      }
    });
  });
}

module.exports = socketMain;
