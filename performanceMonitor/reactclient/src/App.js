import React from "react";
// import io from "socket.io-client";

import "./App.css";


import Widget from "./Widget";

import socket from "./utilities/socketConnection";
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      performanceData: {},
    };
  }

  componentDidMount() {
    socket.on("data", (data) => {
      // console.log(data);

      // Update State for Widgets too Load!
      // Make Copy for current state to Mutate
      const currentState = { ...this.state.performanceData };

      currentState[data.macA] = data;
      this.setState({
        performanceData: currentState,
      });
    });
  }

  render() {
    console.log(this.state.performanceData);
    let widgets = [];
    const data = this.state.performanceData;
    Object.entries(data).forEach(([key, value]) => {
      widgets.push(<Widget key={key} data={value} />);
    });
    return <div className="App">{widgets}</div>;
  }
}

export default App;
