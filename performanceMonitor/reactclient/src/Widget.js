import React, { Component } from "react";
import Cpu from "./Cpu";
import Mem from "./Mem";
import Info from "./Info";

import "./widget.css";

class Widget extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const {
      freeMem,
      osType,
      upTime,
      totalMem,
      usedMem,
      memUsage,
      cpuModel,
      cpuSpeed,
      numberOfCore,
      cpuLoad,
      macA,
      isActive,
    } = this.props.data;

    const cpu = { cpuLoad };
    const mem = { totalMem, usedMem, freeMem, memUsage };
    const info = { macA, osType, upTime, cpuModel, cpuSpeed, numberOfCore };

    let notActiveDiv = "";

    if (!isActive) {
      notActiveDiv = <div className="not-active">Offline</div>;
    }

    const cpuWidgetId = `cpu_widget_${macA.split(":").join("")}`;
    const memWidgetId = `mem_widget_${macA.split(":").join("")}`;

    return (
      <div className="widget col-sm-12">
        {notActiveDiv}
        <h1>Widget</h1>
        <Cpu cpudata={cpu} cpuWidgetId={cpuWidgetId} />
        <Mem memData={mem} memWidgetId={memWidgetId} />
        <Info infoData={info} />
      </div>
    );
  }
}

export default Widget;
