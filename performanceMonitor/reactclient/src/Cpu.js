import React from "react";

import drawCircle from "./utilities/canvasLoadAnimation";

const Cpu = (props) => {
  //   console.log(props);
  const canvas = document.querySelector(`#${props.cpuWidgetId}`);
  drawCircle(canvas, props.cpudata.cpuLoad);

  return (
    <div className="col-sm-3 cpu">
      <h3>CPU Load</h3>
      <div className="canvas-wrapper">
        <canvas id={props.cpuWidgetId} width="200" height="200"></canvas>
        <div className="cpu-text">{props.cpudata.cpuLoad}%</div>
      </div>
    </div>
  );
};

export default Cpu;
